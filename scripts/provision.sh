#!/bin/bash

RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
NC=$(tput sgr0)


# Script to run the initial setup and provisioner.
[ "${UID}" -eq 0 ] || { echo "${RED}Script must be run as root${NC}"; exit 1; }


declare -a REQUIRED

function install_with_zypper {

    zypper refresh
    echo "${REQUIRED[@]}" | xargs zypper install -y
}

echo "${YELLOW}Checking for missing packages...${NC}"

for dep in "ansible" "git";
do
    type -fP "${dep}" &> /dev/null ||  { echo "Missing '${dep}'"; REQUIRED+=("${dep}"); }
done


if [ "${#REQUIRED[@]}" -ne 0 ];
then

    echo "${YELLOW}Installing missing packages...${NC}"

    case "$(lsb_release -si)" in
    "openSUSE") install_with_zypper ;;
    *) echo
        "${RED}Unknown distribution: $(lsb_release -si)${NC}"
        exit 2
        ;;
    esac

    echo "${GREEN}Finished installing missing packages.${NC}"
fi

echo "${GREEN}Running: ${YELLOW}ansible-pull -U https://gitlab.com/chaosmunkey/provisioner.git local.yaml -l $(hostname) -d /opt/provisioner${NC}"

ansible-pull -U https://gitlab.com/chaosmunkey/provisioner-v2.git local.yaml -l "$(hostname)" -d /opt/provisioner
