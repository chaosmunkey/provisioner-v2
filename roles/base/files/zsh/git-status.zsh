function get_git_branch() {
    GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)

    [ -n "${GIT_BRANCH}" ] && echo "(${GIT_BRANCH}) "
}
