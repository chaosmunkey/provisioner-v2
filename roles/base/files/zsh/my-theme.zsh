if [[ "${UID}" -eq 0 ]];
then
    host_colour="red"
    user_symbol="#"
else
    host_colour="green"
    user_symbol="$"
fi

[[ -v SSH_CONNECTION ]] && user_host="%{$terminfo[bold]$fg[${host_colour}]%}%n@%m%{$reset_color%} "


# Code borrowed from:
# https://github.com/ohmyzsh/ohmyzsh/blob/master/themes/fishy.zsh-theme
short_path () {

    local i pwd
    pwd=("${(s:/:)PWD/#$HOME/~}")
    if (( $#pwd > 1 )); then
        for i in {1..$(($#pwd-1))}; do
            if [[ "$pwd[$i]" = .* ]]; then
                pwd[$i]="${${pwd[$i]}[1,2]}"
            else
                pwd[$i]="${${pwd[$i]}[1]}"
            fi
        done
    fi
    echo "${(j:/:)pwd}"
}


short_dir='$(short_path)'
current_dir="%{$terminfo[bold]$fg[blue]%}${short_dir}%{$reset_color%}"


git_status=' %{$fg[yellow]%}$(get_git_branch)%{$reset_color%}'
vi_mode='$(get_vi_mode)'

PS1="${user_host}${current_dir}${git_status}%B${user_symbol} %b"
