" Toggle ':set list' to display 'hidden' characters.
noremap <M-l> :set list!<CR>
noremap <M-m> :MarkdownPreview<CR>
noremap <M-M> :MarkdownPreviewStop<CR>
noremap <M-n> :set number! relativenumber!<CR>
