" ------------------------- "
"      GENERAL SETTINGS     "
" ------------------------- "
syntax enable       " Enables syntax highlighing
set tabstop=4       " Insert 2 spaces for a tab
set shiftwidth=4    " Change the number of space characters inserted for indentation
set smarttab        " Makes tabbing smarter will realize you have 2 vs 4
set expandtab       " Converts tabs to spaces
set smartindent     " Makes indenting smart
set autoindent      " Good auto indent

let c_comment_strings=1 " Revert with ":unlet c_comment_strings".

" Display hidden characters
set lcs=space:·,tab:>-,trail:+
set list
