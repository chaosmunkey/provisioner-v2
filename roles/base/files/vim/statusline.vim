" ------------------------- "
"          COLOURS          "
" ------------------------- "
set laststatus=2

hi User1  ctermbg=black ctermfg=black guibg=#2E3440 guifg=#2E3440
hi User2  ctermbg=black ctermfg=white guibg=#2E3440 guifg=#ECEFF4



hi User8 ctermbg=black ctermfg=red cterm=bold
hi User9  ctermbg=lightgrey ctermfg=red cterm=bold

hi left   ctermbg=darkgrey ctermfg=white
hi leftt  ctermbg=black ctermfg=darkgrey
highlight after_file ctermbg=black     ctermfg=darkblue
highlight file_data  ctermbg=darkblue  ctermfg=white
highlight after_col  ctermbg=darkblue  ctermfg=green guibg=#2E3440 guifg=#A3BE8C
highlight columns    ctermbg=green     ctermfg=black guibg=#A3BE8C guifg=#2E3440
highlight next_right ctermbg=green     ctermfg=red   guibg=#A3BE8C guifg=#BF616A
highlight far_right  ctermbg=red       ctermfg=black guibg=#BF616A guifg=#2E3440



" --------- LEFT ---------- "
set statusline=
set statusline+=%#left#
set statusline+=\ %(%m\ %)   " file modified; add space when flag set
set statusline+=%F\           " filename
set statusline+=%#leftt#%1*

" --------- RIGHT --------- "
set statusline+=%=          " right justify content
set statusline+=%#after_file#
set statusline+=%#file_data#\ %Y\           " file format
set statusline+=%#after_col#
set statusline+=%#columns#%4l/%4L\ %3p%%\   " percentage through the file

set statusline+=%#next_right# " right chevron looking thing
