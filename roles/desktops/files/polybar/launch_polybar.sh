#!/usr/bin/env bash

pgrep polybar &> /dev/null && notify-send "Restarting polybar..."

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use
# polybar-msg cmd quit

# Start the bars, please!
polybar da-bar
